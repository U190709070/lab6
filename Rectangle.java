
public class Rectangle {
    int sideA, sideB;
    Point topleft;

    public Rectangle(int sideA, int sideB, Point topleft) {
        this.sideA = sideA;
        this.sideB = sideB;
        this.topleft = topleft;
    }

    public int area (){
        return this.sideA * this.sideB;
    }
    public int perimeter(){
        return 2*(this.sideA + this.sideB);
    }
    public String corners(){
        Point lowleft = new Point(this.topleft.xCoord, this.topleft.yCoord - sideB );
        Point topright = new Point(this.topleft.xCoord + sideA, this.topleft.yCoord);
        Point lowright = new Point(topright.xCoord, topright.yCoord - sideB);
        return  "("+topleft.xCoord+","+ topleft.yCoord+"),"+"("+lowleft.xCoord+"," + lowleft.yCoord+"),"+"("+lowright.xCoord+"," + lowright.yCoord+"),"+"("+topright.xCoord+"," + topright.yCoord+")";
    }


}

