public class Circle {
    int radius;
    Point center;

    public Circle(int radius, Point center) {
        this.radius = radius;
        this.center = center;
    }

    public double area (){
        return Math.PI * (this.radius*this.radius);
    }
    public double perimeter(){
        return Math.PI * 2 * this.radius;
    }
    public boolean intersect(Circle obj){
        int x_yDif = (this.center.xCoord - obj.center.xCoord) * (this.center.xCoord - obj.center.xCoord) + (this.center.yCoord - obj.center.yCoord) * (this.center.yCoord - obj.center.yCoord);
        int rad = (this.radius + obj.radius) * (this.radius + obj.radius);
        if (x_yDif > rad) {
            return false;
        }else {
            return true;
        }

    }
}