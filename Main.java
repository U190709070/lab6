

public class Main {
    public static void main(String[] args) {
        Rectangle rectangle = new Rectangle(3,4, new Point(5,6));
        System.out.println("Rectangle Area: " + rectangle.area());
        System.out.println(("Rectangle Perimeter: " + rectangle.perimeter()));
        System.out.println("Rectangle Corners: " + rectangle.corners());
        Circle circle = new Circle(10, new Point(2,3));
        Circle circle1 = new Circle(5, new Point(4,3));
        System.out.println("Circle Area: " + circle.area());
        System.out.println("Circle Perimeter: " + circle.perimeter());
        System.out.println("Intersect: " + circle.intersect(circle1));
    }

}

